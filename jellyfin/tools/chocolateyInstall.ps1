$ErrorActionPreference = 'Stop'

function CreateShortcut {
    param([String] $Path)

    $shortcutArgs = @{
        shortcutFilePath = "$Path\Jellyfin.lnk"
        targetPath       = 'http://localhost:8096'
        iconLocation     = "$ENV:ProgramFiles\Jellyfin\Server\jellyfin-web\favicon.ico"
    }

    Install-ChocolateyShortcut @shortcutArgs
}

$v = "10.7.7"

$packageArgs = @{
    packageName    = 'jellyfin'
    fileType       = 'EXE'
    url64bit       = "https://repo.jellyfin.org/releases/server/windows/versions/stable/installer/${v}/jellyfin_${v}_windows-x64.exe"
    checksum64     = '8ebbe13f21c1fd16238e7611ca109e271408d778a91bbf2f313b4c5546df1de1'
    checksumType64 = 'sha256'
    silentArgs     = '/S'
    validExitCodes = @(0,1)
    softwareName   = 'Jellyfin Server*'
}
 
Install-ChocolateyPackage @packageArgs

CreateShortcut -Path "$ENV:Public\Desktop"
CreateShortcut -Path "$ENV:ProgramData\Microsoft\Windows\Start Menu\Programs"
